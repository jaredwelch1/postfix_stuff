#!/usr/local/bin/python3

import sys
import subprocess
import traceback
from subprocess import Popen, PIPE
import logging
import email
import re
import datetime

from web_util.db_connect import DBConnection
import db_config as db_conf

def get_plaintext(email_message):
    for item in email_message.walk():
        if item.get_content_type() == 'text/plain':
            return item

def parse_purchase_info(usbank_email):
    e = email.message_from_string(usbank_email)
    plaintext = get_plaintext(e)
    body = plaintext._payload
    purchase_re = r"Your purchase[\sa-zA-Z]+\$(?P<amount>[\d,]+\.[\d]+) at (?P<name>[\d\sa-zA-Z#\-\*\=\'\\&#\/]+)\."
    card_info_re = r'purchase[\sa-zA-Z]+(?P<card_type>debit|credit) card.*(?P<card_number>[\d]{4})\.'

    purchase_info = re.search(purchase_re, body)
    card_info = re.search(card_info_re, body)

    if purchase_info:
        purchase_info = purchase_info.groups()
    else:
        raise ValueError('Cannot parse purchase_info')

    if card_info:
        card_info = card_info.groups()
    else:
        raise ValueError('Cannot parse card_info')

    return ('PURCHASE',) + purchase_info + card_info

def parse_transaction_info(email_str):
    e = email.message_from_string(email_str)
    plaintext = get_plaintext(e)
    body = plaintext._payload

    tx_re = r'Your transaction[a-zA-Z ]+\$(?P<amount>[\d\.,]+)[a-zA-Z ]+complete\.?'
    acct_re = r'account[a-zA-Z\s]+(?P<acct>[\d]{4})'
    dep_re = r'Your [Dd]eposit[a-zA-Z ]+\$([\d\.,]+)[a-zA-Z ]+complete\.?'

    tx_info = re.search(tx_re, body)
    dep_info = re.search(dep_re, body)
    acct_info = re.search(acct_re, body)
    tx_details = tx_info if tx_info else dep_info

    if tx_details:
        tx_details = tx_details.groups()
    else:
        raise ValueError('cannot parse transaction details')
    if acct_info:
        acct_info = acct_info.groups()
    else:
        raise ValueError('cannot parse account information')

    alert_type = 'TRANSACTION' if tx_info else 'DEPOSIT'

    return (alert_type,) + tx_details + ('','') + acct_info

def parse_cc_transaction_info(email_str):
    e = email.message_from_string(email_str)
    pt = get_plaintext(e)
    body = pt._payload

    cc_purch_re = r'Your credit card[ a-zA-Z]+\$(?P<amount>[\d\.,]+) at (?P<name>[\d \*a-zA-Z&\-]+)\.'
    card_re = r'card[ a-zA-Z]+in (?P<card_number>[\d]{4})'

    purch_info = re.search(cc_purch_re, body)
    cc_info = re.search(card_re, body)

    if purch_info:
        purch_info = purch_info.groups()
    else:
        raise ValueError('cant parse cc charge info')
    if cc_info:
        cc_info = cc_info.groups()
    else:
        raise ValueError('cant parse cc card info')
    return ('CREDITCARD',) + purch_info + ('credit',) + cc_info

def get_subject(email_string):
    return parse_subject(email_string)

def get_from(email_string):
    return parse_from(email_string)

def parse_subject(body_str):
    subj_re = r"[S]ubject: (?P<subject>[\da-zA-Z\s\:\.\-\*#\'\\\/&]+)\.?\n"
    return re.search(subj_re, body_str).groups()[0]

def parse_from(body_str):
    from_re = r'[F]rom: (?P<from>[\da-zA-Z\s@\.\<\>\+\_\=\-\"]+)\n'
    return re.findall(from_re, body_str)[0]

def get_alert_type(subject):
    if 'transaction' in subject.lower():
        if 'credit card' in subject.lower():
            return 'CC_TRANSACTION'
        else:
            return 'TRANSACTION'
    if 'purchase' in subject.lower():
        return 'PURCHASE'

    if 'request' in subject.lower():
        return 'REQUEST'

def _clean_str(db_tup_str):
    for c in [',',"'"]:
        db_tup_str = db_tup_str.replace(c,'')
    return db_tup_str
    
def insert_alert_tuple(row_tuple):
    cxn = DBConnection(db_conf.DB, db_conf.USER, db_conf.PW)
    logging.info('inserting:')
    if len(row_tuple) is not 5:
        raise ValueError(f'Insert tuple not valid length: {row_tuple}')
    d = datetime.datetime.now().date().strftime('%Y-%m-%d')
    row_tuple += (False,d)
    row_tuple = tuple([_clean_str(v) if type(v) is str else v for v in row_tuple])
    qry = f"insert into banking_alerts (type, amount, name, charge_type, account, is_complete, insert_date) VALUES {row_tuple};"
    cxn.query(qry)

def handle_usbank_email(subject, email_s):
    alert_type_s = get_alert_type(subject)

    if alert_type_s == 'TRANSACTION':
        tx_tuple = parse_transaction_info(email_s)
        logging.debug(tx_tuple)
        insert_alert_tuple(tx_tuple)
    elif alert_type_s == 'PURCHASE':
        px_tuple = parse_purchase_info(email_s)
        logging.debug(px_tuple)
        insert_alert_tuple(px_tuple)
    elif alert_type_s == 'CC_TRANSACTION':
        tx_tuple = parse_cc_transaction_info(email_s)
        logging.debug(tx_tuple)
        insert_alert_tuple(tx_tuple)
    elif alert_type_s == 'REQUEST':
        logging.debug('you did not error but cant process requests yet')
    else:
        logging.error(f'Cannot parse alert type from email: {subject}')
        raise ValueError('unknown alert type')
    # we can discard us bank emails we do not want to get them in mailbox
    sys.exit(0)

def apply_filter(email_str, args):
    from_s = get_from(email_str)
    subj_s = get_subject(email_str)

    if '1800usbanks@alerts.usbank.com' in from_s.lower() or 'jared.welch1' in from_s.lower():
    # if '1800usbanks@alerts.usbank.com' in from_s.lower():
        logging.debug('inside the usbank handler')
        handle_usbank_email(subj_s, email_str)
    else:
        logging.debug(f'Un recognized sender: {from_s}. Exiting...')
        sys.exit(0)

def init_log(log_level=logging.WARNING):
    logging.basicConfig(filename='/home/jared/email_filter.log', level=log_level)

def dump_email(email_s):
    with open('/home/jared/.email_filter_cache', 'a') as fp:
        fp.write(email_s)

def main(email_in):
    sent_to = sys.argv[3].lower()
    from_email = sys.argv[1].lower()
    args = sys.argv

    dump_email(email_in)
    # handle emails and exit early unless email passes

    whitelist = ['jared.welch1@gmail.com',
                 'jared.welch1+caf_=jared=ec2-35-164-234-128.us-west-2.compute.amazonaws.com@gmail.com']

    logging.debug(f'from: {from_email}')
    if from_email in whitelist:
        apply_filter(email_in, args)
        logging.debug(f'filter passed for email from: {args[1]}')
        p = Popen(['/usr/sbin/sendmail', '-G', '-i', sent_to], stdin=PIPE)
        stdout, stderr = p.communicate(email_in.encode())
        ret = p.wait()
    else:
        logging.info(f'Invalid sender: {from_email}')
        ret = 0
    # return code based on sendmail output
    sys.exit(ret)

if __name__ == '__main__':
    try:
        init_log(log_level=logging.DEBUG)
        email_in = sys.stdin.read()
        main(email_in)
    except Exception as e:
        logging.error(f'General Exception:')
        logging.error(f'{traceback.format_exc()}')
        er_email_filename = f"/home/jared/postfix_stuff/exception_email_{datetime.datetime.now().strftime('%Y%m%d%H%M')}"
        with open(er_email_filename, 'w') as error_email_fp:
            error_email_fp.write(email_in)
        sys.exit(0)
